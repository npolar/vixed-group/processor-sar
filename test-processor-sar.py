#!/usr/bin/env python3

import sys
from pyproj import Proj, transform
import rasterio
import json
import numpy as np
from sentinelsat import geojson_to_wkt
from shapely import wkt
from shapely.geometry import Point, Polygon
import geojson
import xarray as xr

settings_bounds = sys.argv[1]
result_bounds = sys.argv[2]

f = open(settings_bounds)
sbounds = json.load(f)
scrs = sbounds['crs']

roi = geojson_to_wkt(sbounds['roi'])
roi_feature = geojson.Feature(geometry=wkt.loads(roi), properties={})
scoords = roi_feature['geometry']['coordinates'][0]

with rasterio.open(result_bounds) as src:
    bounds = src.bounds
    corners = (
        (bounds.left, bounds.top),
        (bounds.right, bounds.top),
        (bounds.right, bounds.bottom),
        (bounds.left, bounds.bottom)
    )
    array = src.read(1)
    crs = src.crs
    nodata = src.nodata #Nodata value of raster image

proj_geo = Proj(scrs) # scrs - setting crs

rpoly = Polygon(corners)

scoords = [proj_geo(i[0], i[1]) for i in scoords]
spoly = Polygon(scoords)

# Assert whether polygons overlap, i.e. have more than one but not all points in common
# As the coordinates are slightly shifted during the generation of the geotiff image
# in processor-sar.py, this assertion serves its purpose for now.
#TODO: find a more accurate way of testing the degree to which the polygons overlap
print(spoly)
print("------\n")
print(rpoly)
print("------\n")

assert spoly.overlaps(rpoly), "Settings polygon is not contained within result polygon"

# src.nodata is a numerical value (0.0). If the array sum equal, we have no data
assert array.sum != nodata, "Image contains no data"
