#!/bin/bash
set -x # quit on any errors

# USAGE:
#./test-processor.sh output.tif settings.json

# execute processor with settings.json as input and out.tif as output
OUTPUT_FILE=$1 # first argument to this script
SETTINGS_FILE=$2 # second argument

# path to processor can be either full or relative (relative to this script)
# relative is probably better, so script can be run on various machines
# if this script and processor are in the same directory then ./processor-sar.py will do
PROCESSOR=./processor-sar.py

# Run python executable (processor)
GDAL_DATA=/usr/share/gdal SHUB_USER=$SHUB_USER SHUB_PASS=$SHUB_PASS $PROCESSOR --output-file=$OUTPUT_FILE --log-file=${OUTPUT_FILE%.*}-log.txt --request-file=$SETTINGS_FILE -k

# Run test script
# script should exit with status code 1 in case of failure
# like sys.exit(1) or raise exception
./test-processor-sar.py $SETTINGS_FILE ${OUTPUT_FILE}
