#!/usr/bin/env python3

import re
import argparse
import datetime
import glob
import os
import subprocess
from zipfile import ZipFile
import multiprocessing
from functools import partial
import logging
import shutil
import json
import sys
from sentinelsat import SentinelAPI, geojson_to_wkt
import rasterio
from pyproj import Proj
import affine
import numpy
from urllib import request as rq
import tempfile
from dateutil import parser as date_parser
import numpy as np

def convert_coords(coords_list, crs):
    """
    Converts requested coordinates to requested projection (crs)
    """
    #src_proj = Proj(init='EPSG:4326')
    trg_proj = Proj(init=crs)
    xy_list = [trg_proj(c[0], c[1]) for c in coords_list[0]]
    x_array = numpy.array([c[0] for c in xy_list])
    y_array = numpy.array([c[1] for c in xy_list])
    return x_array, y_array

def affine_transformation(coords_list, res, crs):
    """
    Creates affine-style geotransformation
    """
    a_pixel_width = res
    b_rotation = 0
    d_column_rotation = 0
    e_pixel_height = res
    crs = str(crs).upper()
    (x_array, y_array) = convert_coords(coords_list, crs)
    c_x_ul = x_array.min()
    f_y_ul = y_array.max()
    height = (y_array.max() - y_array.min()) / res
    width = (x_array.max() - x_array.min()) / res

    aff = affine.Affine(a_pixel_width,
                 b_rotation,
                 c_x_ul,
                 d_column_rotation,
                 -1 * e_pixel_height,
                 f_y_ul)
    return aff, height, width

def create_empty_dst(fpath, coords_list, res, crs, dtype):
    """
    Creates geotiff-dataset as boundaries for the 
    satellite data
    """
    aff, height, width = affine_transformation(coords_list, res, crs)
    dst = rasterio.open(fpath,
                    'w',
                    driver='GTiff',
                    height=height,
                    width=width,
                    count=1,
                    dtype=dtype,
                    crs=crs,
                    transform=aff,
                    nodata=0,
                    compress="LZW",
                    predict=2
                    )
    return dst

def percentile_peak(ifile):
    with rasterio.open(ifile, 'r') as dst:
        ubound = numpy.percentile(dst.read(1), 99)
    return ubound

def process_sentinel_scene(product, data_dir, output_filepath):
    """
    Extract the contents of the obtained file
    and put it in the right place
    """
    with ZipFile(os.path.join(data_dir, ".".join([product['identifier'], "zip"]))) as zf:
        zf.extractall(data_dir)
        input_paths = glob.glob(os.path.join(data_dir, ".".join([product['identifier'],"SAFE/measurement/*.tiff"])))
        for i, ifile in enumerate(input_paths):
            if re.search(r".*(vh).*.tiff|.*(hh).*.tiff", ifile):
                with tempfile.NamedTemporaryFile() as temp:
                    uboundval = percentile_peak(ifile)
                    subprocess.call([
                        "gdal_translate",
                        "-scale",
                        "0", "{}".format(int(uboundval)), "0", "255",
                        "-ot", "Byte",
                        ifile,
                        temp.name])

                    subprocess.call([
                        "gdalwarp",
                        "-t_srs",
                        request["crs"],
                        "-srcnodata",
                        "0",
                        "-dstnodata",
                        "0",
                        "-r",
                        "bilinear",
                        temp.name,
                        output_filepath
                    ])


def download_sentinel_product(
                        product_key,
                        products,
                        api,
                        request,
                        output_path
                    ):

    print("Obtaining item {}".format(product_key))
    api.download(product_key, directory_path=output_path)



def remove_dir(path):
    for root, dirs, files in os.walk(path):
        for f in files:
            os.unlink(os.path.join(root, f))
        for d in dirs:
            shutil.rmtree(os.path.join(root, d))
        os.rmdir(root)



def main():

    p = argparse.ArgumentParser()
    p.add_argument("--log-file", default=None)
    p.add_argument("--output-file", default=None)
    p.add_argument("--request-file", default=None)
    p.add_argument("--download-dir", default=None)
    p.add_argument("--cleanup-dir", default=True, action="store_true")
    p.add_argument("-k", "--keep-temporary", action="store_true")
    p.add_argument("--download-only", action="store_true")

    args = p.parse_args()

    SHUB_USER = os.environ['SHUB_USER']
    SHUB_PASS = os.environ['SHUB_PASS']

    global logger

    logger = logging.getLogger('vixed')
    logger.setLevel("CRITICAL")

    if args.request_file is not None:
        global request
        request = json.loads(open(args.request_file).read())
    else:
        logger.info("Request file missing, aborting")
        raise Exception("Can't proceed without request json file")

    api_url = 'https://colhub.met.no/'
    api = SentinelAPI(SHUB_USER, SHUB_PASS, api_url=api_url, show_progressbars=True)

    footprint = geojson_to_wkt(request['roi'])
    logger.debug("Search footprint is: {}".format(footprint))

    try:
        if request['end_time']:
            end_time = date_parser.parse(request['end_time'])
    except:
        end_time = datetime.datetime.utcnow()

    start_time = end_time - datetime.timedelta(hours=request['time_delta_hours'])

    # query dhus
    products = api.query(
        footprint,
        date = (start_time, end_time),
        platformname=request['platformname'],
        producttype = request['producttype'],
        sensoroperationalmode=request['sensoroperationalmode']
    )

    # process quering results
    n_scenes = len(products.keys())
    message = "Found {} scenes".format(n_scenes)
    logger.info(message)
    if n_scenes>0:
        logger.info("Scenes list:\n{}".format(
            "\n".join(['\t'+products[key]['identifier'] for key in products.keys()])))
    else:
        logger.debug(" ".join(['No satellites scenes found',
                              'within last {} hours.'.format(request["time_delta_hours"])]))
        raise SystemExit("Aborting.")

    output_path = args.output_file
    logger.debug("Output path: {}".format(output_path))
    if args.download_dir is None:
        data_dir = os.path.splitext(output_path)[0] + ".d"
    else:
        data_dir = args.download_dir

    logger.debug("Download data directory: {}".format(data_dir))

    if not os.path.exists(data_dir):
        os.mkdir(data_dir)

    logger.debug("Begin download")
    pool = multiprocessing.Pool(processes=1)
    output = pool.map(partial(download_sentinel_product,
                              products=products,
                              api=api,
                              output_path=data_dir,
                              request=request), products.keys())

    if args.download_only is True:
        logger.info("Asked to only download data, stopping")
        sys.exit()

    # Process obtained files with gdal
    with tempfile.NamedTemporaryFile() as tfile:

        create_empty_dst(
            tfile.name,
            request['roi']['coordinates'],
            request['spatial_resolution'],
            request['crs'],
            rasterio.uint8
        )

        for product_key in products.keys():
            process_sentinel_scene(products[product_key], data_dir, tfile.name)

        subprocess.call([
            "gdal_translate",
            "-ot", "Byte",
            "-co", "COMPRESS=JPEG",
            "-co", "JPEG_QUALITY=70",
            "-scale",
            tfile.name,
            output_path
            ])

    try:
        if request['plot_data'] == True:
            plot_gtiff(output_path)
    except KeyError:
        pass
    except ValueError:
        warnings.warn("Unknown value for key plot_data, not plotting")

    if args.keep_temporary is False:
        logger.info('Removing temporary directory {}'.format(data_dir))
        remove_dir(data_dir)

if __name__ == "__main__":
    main()
